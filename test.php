<?php

namespace App\Action\Netex\Download;

use App\Action\ActionInterface;
use App\Extractor\Files\FileExtractorInterface;
use App\Model\Provider\Netex\Netex;
use Psr\Log\LoggerInterface;
use Symfony\Component\Finder\Finder;

class ExtractDataFiles implements ActionInterface
{
    private LoggerInterface $logger;
    private Netex $netex;
    private FileExtractorInterface $gzipExtractor;
    private string $projectRoot;

    public function __construct(
        Netex $netex,
        LoggerInterface $logger,
        FileExtractorInterface $gzipExtractor,
        string $projectRoot
    ) {
        $this->logger = $logger;
        $this->netex = $netex;
        $this->gzipExtractor = $gzipExtractor;
        $this->projectRoot = $projectRoot;
    }

    public function execute(): void
    {
        $downloadFilePath = sprintf(
            '%s/%s',
            $this->projectRoot,
            $this->netex->getTargetDirectory()
        );

        $finder = new Finder();
        $finder->files()->in($downloadFilePath);

        if (!$finder->hasResults()) {
            throw new \Exception('No downloaded files found!');
        }

        foreach ($finder as $file) {
            $this->logger->info('Extracting file', ['file' => $file->getPathname()]);
            $this->gzipExtractor->extract($file->getPathname(), $this->netex);
        }
    }
}
