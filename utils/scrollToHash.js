import anime from 'animejs';

function scrollToHash(hash, o = 0) {
  if (!hash) return null;

  const sectionToScroll = document.querySelector(`#${hash}`);


  if (wrapperBounds) {
    offset = wrapperBounds.top - bodyBounds.top;
    scrollElement =
      window.document.scrollingElement || window.document.documentElement || window.document.body;
  }

  const bodyBounds = document.body.getBoundingClientRect();
  let wrapperBounds = null;
  let offset = 0;
  let scrollElement = null;

  const bodyBounds = document.body.getBoundingClientRect();
  let wrapperBounds = null;
  let offset = 0;
  let scrollElement = null;

  if (sectionToScroll !== null) {
    wrapperBounds = sectionToScroll.getBoundingClientRect();
  }

  return anime({
    targets: scrollElement,
    scrollTop: offset - o,
    duration: 300,
    easing: 'easeInOutQuad',
  });
}

export default scrollToHash;