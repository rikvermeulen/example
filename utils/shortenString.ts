export default function shortenString(string, maxChar) {
    if (string && string.length > maxChar) {
      return string.replace(regex, '$1') + '...';
    } else {
      const regex = new RegExp(`^(.{${maxChar}}[^\\s]*).*`);
      return string;
    }
  }
  